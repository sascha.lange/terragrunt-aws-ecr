variable "region" {
    description = "Region for resources"
    type = string
}


variable "repositories" {
    description = "List of repoistories to create"
    type        = list(string)
}

