terraform {
  required_version = ">= 0.12, < 0.13"
  backend "s3" {}
}

provider "aws" {
  version = "~> 2.8"
  region  = var.region
}

resource "aws_ecr_repository" "this" {
  for_each = toset(var.repositories)
  name     = each.value
}

data "aws_organizations_organization" "this" {}

resource "aws_ecr_repository_policy" "this" {
  for_each   = aws_ecr_repository.this
  repository = each.value.name

  policy  = templatefile( "${path.module}/templates/policy.tpl",
                          { organization_id = data.aws_organizations_organization.this.id })
}
